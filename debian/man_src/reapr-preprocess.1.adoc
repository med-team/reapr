# reapr-preprocess(1)

## NAME

reapr-preprocess - preprocess files for REAPR run

## SYNOPSIS

*reapr preprocess* <assembly.fa> <in.bam> <outputdir>

## SEE ALSO

reapr(1)
