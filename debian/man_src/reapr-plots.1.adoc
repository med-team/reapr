# reapr-plots(1)

## NAME

reapr-plots - makes Artemis plot files for a given contig

## SYNOPSIS

*reapr plots* [options] <in.stats.gz> <outprefix> <assembly.fa> <contigid>

## OPTIONS

*-s* <prefix>::
  This should be the outfiles prefix used when *score* task was run

## SEE ALSO

reapr(1)
